"""
Test transfer_vn main.
"""
from pathlib import Path
from unittest.mock import patch

import pytest
from vn_chk_altitude import __version__, main


def test_version():
    """Check if version is defined."""
    assert __version__ == "0.1.0"
    with patch("sys.argv", ["py.test", "--version"]):
        with pytest.raises(SystemExit):
            main.run()

