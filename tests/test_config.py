"""
Test each property of envconf module.
"""
import logging
from pathlib import Path

import pytest

from vn_chk_altitude import __version__
from vn_chk_altitude.config import Config
from vn_chk_altitude.env import ENVDIR


# Testing constants
CFG_FILE = "cfg_test.toml"
CFG = Config(CFG_FILE).settings


def test_site():
    """Check if configuration file exists."""
    cfg_file = ENVDIR / CFG_FILE
    assert cfg_file.is_file()


def test_db():
    """ Test db settings."""
    assert CFG.db.db_host == "localhost"
    assert CFG.db.db_port == 5432
    assert CFG.db.db_user == "<dbUser>"
    assert CFG.db.db_password == "<dbPassword>"
    assert CFG.db.db_name == "<dbName>"
    assert CFG.db.db_schema == "schema"
    assert CFG.db.db_querystring.sslmode == "prefer"