#!/usr/bin/env python3
"""
Script checking VisioNature observation altitude :
- Prerequisite: 
  - Observations must be in Postgres DB, see Client-API_VN
  - DEM raster must be available with the same projection
- Script extracts observation from PG DB and compares to DEM


"""
import argparse
import logging
import logging.config
import shutil
import sys
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
from subprocess import call

import pkg_resources
from toml import TomlDecodeError

from .get_altitude import GetAltitude

from . import _, __version__, logger
from .config import Config
from .env import ENVDIR, LOGDIR, RESULTDIR
from .get_observations import GetObservations


def arguments(args):
    """Define and parse command arguments.
    Args:
        args ([str]): command line parameters as list of strings
    Returns:
        :obj:`argparse.Namespace`: command line parameters namespace
    """
    # Get options
    parser = argparse.ArgumentParser(description="vn_chk_altitude client app")
    parser.add_argument(
        "-V",
        "--version",
        help=_("Print version number"),
        action="version",
        version="%(prog)s {version}".format(version=__version__),
    )
    out_group = parser.add_mutually_exclusive_group()
    out_group.add_argument(
        "-v",
        "--verbose",
        help=_("Increase output verbosity"),
        action="store_true",
    )
    out_group.add_argument(
        "-q", "--quiet", help=_("Reduce output verbosity"), action="store_true"
    )
    parser.add_argument(
        "--init",
        help=_("Initialize the TOML configuration file"),
        action="store_true",
    )
    parser.add_argument(
        "--edit",
        help=_("Edit the TOML configuration file in default editor"),
        action="store_true",
    )
    parser.add_argument("file", help="Configuration file name")

    return parser.parse_args(args)


def init(file: str) -> None:
    """Init config file from template
    Args:
        file (str): [description]
    """

    toml_src = pkg_resources.resource_filename(__name__, "data/config.toml")
    ENVDIR.mkdir(exist_ok=True)
    logger.info(_("Conf directory %s created, if needed"), str(ENVDIR))
    toml_dst = str(ENVDIR / file)
    overwrite = "y"
    if Path(toml_dst).is_file():
        logger.warning(_("%s file already exists"), toml_dst)
        overwrite = input(
            _("Would you like to overwrite file {}: (Yes/[No])?").format(toml_dst)
        )
    if overwrite.lower()[0:1] != "y":
        logger.warning(_("File %s will be preserved"), toml_dst)
    else:
        logger.warning(_("file %s will be overwritten"), toml_dst)
        logger.info(
            _("Creating TOML configuration file %s, from %s"), toml_dst, toml_src
        )
        shutil.copyfile(toml_src, toml_dst)
    logger.info(_("Please edit %s before running the script"), toml_dst)

    return None


def edit(file: str) -> None:
    """Open editor to edit config file
    Args:
        file (str): [description]
    """
    config_file = ENVDIR / file
    if Path(config_file).is_file():
        call(["editor", config_file])
    else:
        logger.warning(_("File %s does not exist"), config_file)

    return None


def main(args):
    """Main entry point allowing external calls
    Args:
      args ([str]): command line parameter list
    """
    # Create directories if not exist
    LOGDIR.mkdir(parents=True, exist_ok=True)
    RESULTDIR.mkdir(parents=True, exist_ok=True)

    # create file handler which logs even debug messages
    fh = TimedRotatingFileHandler(
        str(LOGDIR / (__name__ + ".log")),
        when="midnight",
        interval=1,
        backupCount=100,
    )
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(module)s:%(funcName)s - %(message)s"
    )
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    # Get command line arguments
    args = arguments(args)

    # Define verbosity
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    elif args.quiet:
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.INFO)

    logger.info("%s, version %s", sys.argv[0], __version__)
    logger.info("Arguments: %s", sys.argv[1:])

    # If required, first create YAML file
    if args.init:
        logger.info("Creating TOML configuration file")
        init(args.file)
        return None

    # If required, first create YAML file
    if args.edit:
        logger.info("Editing TOML configuration file")
        edit(args.file)
        return None

    # Get configuration from file
    if not (ENVDIR / args.file).is_file():
        logger.critical("Configuration file %s does not exist", str(ENVDIR / args.file))
        return None
    cfg = Config(args.file).settings
    it_obs = GetObservations(cfg)
    obs_alti = GetAltitude(cfg)

    try:
        while True:
            obs = next(it_obs)
            obs_alti.check(obs)
    except StopIteration:
        return


def run():
    """Zero-argument entry point for use with setuptools/distribute."""
    # raise SystemExit(main(sys.argv))
    return main(sys.argv[1:])


if __name__ == "__main__":
    run()
