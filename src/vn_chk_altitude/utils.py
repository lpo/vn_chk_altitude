"""Some utility functions"""


def coalesce_in_dict(source: dict, key: str, default: dict) -> dict:
    """[summary]
    Args:
        source (dict): [description]
        key(str):
        default (any): [description]
    Returns:
        any: [description]
    """
    if key in source:
        return source[key]
    else:
        return default