"""Outil d'import de données entre instances GeoNature (côté client)"""

import gettext
from pathlib import Path
import logging
from pkg_resources import DistributionNotFound, get_distribution

try:
    dist_name = "vn_chk_altitude"
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:  # pragma: no cover
    __version__ = "unknown"
finally:
    del get_distribution, DistributionNotFound

# Install gettext for any file in the application
localedir = Path(__file__).resolve().parent / "locale"
gettext.bindtextdomain("vn_chk_altitude", str(localedir))
gettext.textdomain("vn_chk_altitude")
_ = gettext.gettext

logger = logging.getLogger(__name__)