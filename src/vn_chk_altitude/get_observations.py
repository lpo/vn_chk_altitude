"""Read observations from PG database."""
from typing import Any, Tuple

import pandas
from dynaconf import Dynaconf


import psycopg2


from . import _, __version__, logger


class GetObservations:
    def __init__(self, cfg: Dynaconf) -> None:
        self._cfg = cfg
        # Getting communes (down sampled)
        logger.info(_("Connecting to database %s:%s"), cfg.db.db_host, cfg.db.db_name)
        self._con = psycopg2.connect(
            database=cfg.db.db_name,
            user=cfg.db.db_user,
            password=cfg.db.db_password,
            host=cfg.db.db_host,
            port=cfg.db.db_port,
        )
        sql = (
            "SELECT site, id, id_canton, name FROM "
            + str(self._cfg.db.db_schema)
            + ".local_admin_units"
        )
        self._communes = (
            pandas.read_sql(sql, self._con)
            # .groupby("id_canton")
            # .sample(n=2)  # TO BE REMOVED
            .sort_values(by=["id_canton", "id"])
        )
        self._i_c = 0

        return None

    def __iter__(self) -> Any:
        return self

    def __next__(self) -> pandas.DataFrame:
        logger.info(
            _("%s - Getting observations from %s (%s) in department id %s"),
            self._i_c,
            self._communes.iloc[self._i_c]["name"],
            self._communes.iloc[self._i_c]["id"],
            self._communes.iloc[self._i_c]["id_canton"],
        )
        sql = (
            "SELECT o.site, o.id_universal, o.date_year, o.altitude, o.coord_x_local, o.coord_y_local, l.name, l.id_canton FROM "
            + str(self._cfg.db.db_schema)
            + ".observations o"
            + " INNER JOIN "
            + str(self._cfg.db.db_schema)
            + ".places p ON (p.site = o.site) AND (p.id = o.id_place)"
            + " INNER JOIN "
            + str(self._cfg.db.db_schema)
            + ".local_admin_units l ON (l.site = o.site) AND (l.id = p.id_commune)"
            + " WHERE (l.id = "
            + str(self._communes.iloc[self._i_c]["id"])
            + ")"
        )
        obs = pandas.read_sql(sql, self._con)
        if self._i_c < (len(self._communes) - 1):
            self._i_c += 1
        else:
            logger.info(_("Stopping iteration at end of list of local_admin_units"))
            raise StopIteration
        return obs
