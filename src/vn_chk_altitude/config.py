"""Configuration settings loader."""

from dynaconf import Dynaconf, Validator

from . import _, __version__, logger
from .env import ENVDIR


class Config:
    """Read config file and expose settings"""

    def __init__(self, file: str) -> None:
        """[summary]
        Args:
            file (str): Configuration file name
        """

        p = ENVDIR / file
        logger.info(_("Loading TOML configuration %s"), p)
        self.settings = Dynaconf(
            settings_files=[p],
            validators=[
                 Validator('db.db_host', must_exist=True),
                 Validator('db.db_port', must_exist=True, gt=0),
                 Validator('db.db_user', must_exist=True),
                 Validator('db.db_password', must_exist=True),
                 Validator('db.db_name', must_exist=True),
                 Validator('db.db_schema', must_exist=True),
            ]
        )
