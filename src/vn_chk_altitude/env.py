"""Environment variables file"""

from pathlib import Path

ENVDIR = Path.home() / ".vn_chk_altitude"
"""Config system directory (~/.vn_chk_altitude/)"""

LOGDIR = ENVDIR / "log"
"""Log system directory (subdir of ENVDIR)"""

RESULTDIR = ENVDIR / "result"
"""Directory for resulting csv files (subdir of ENVDIR)"""