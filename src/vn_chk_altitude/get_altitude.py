"""Get altitude from observation."""
import math
import os
from logging import Logger

import numpy
import pandas
import psycopg2
import rasterio
from dynaconf import Dynaconf

from . import _, __version__, logger
from .env import ENVDIR, LOGDIR, RESULTDIR


class GetAltitude:
    def __init__(self, cfg: Dynaconf) -> None:
        self._cfg = cfg
        self._id_dept = None
        # Getting departments (down sampled)
        logger.info(_("Connecting to database %s:%s"), cfg.db.db_host, cfg.db.db_name)
        self._con = psycopg2.connect(
            database=cfg.db.db_name,
            user=cfg.db.db_user,
            password=cfg.db.db_password,
            host=cfg.db.db_host,
            port=cfg.db.db_port,
        )
        sql = (
            "SELECT DISTINCT id, short_name, name FROM "
            + str(self._cfg.db.db_schema)
            + ".territorial_units"
        )
        self._departments = pandas.read_sql(sql, self._con).sort_values(by=["id"])
        print(self._departments)
        return None

    def __alti(self, x: float, y: float) -> float:
        try:
            alti = self._dem_b1[self._dem.index(x, y)]
        except IndexError:
            logger.error(_("Observation out of range (%s, %s)"), x, y)
            alti = -1
        return alti

    def __slope(self, x: float, y: float) -> float:
        try:
            slope = 100 * math.tan(math.radians(self._slope_b1[self._slope.index(x, y)]))
        except IndexError:
            logger.error(_("Observation out of range (%s, %s)"), x, y)
            slope = -1
        return slope

    def check(self, obs: pandas.DataFrame) -> None:
        if len(obs) > 0:
            id_dept = self._departments[
                self._departments.id == obs.iloc[0]["id_canton"]
            ].short_name.values[0]
            commune = obs.iloc[0]["name"]
            logger.info(
                _("Processing %s observations in department %s, commune %s"),
                len(obs),
                id_dept,
                commune,
            )
            if self._id_dept != id_dept:
                self._id_dept = id_dept
                logger.info(_("Reading DEM & SLOPE rasters for region %s"), self._id_dept)
                self._dem = rasterio.open(
                    f"/mnt/data/SIG/DEM/RGEALTI/RGEALTI_2-0_5M_ASC_LAMB93-IGN69_D0{id_dept}.tif"
                )
                self._dem_b1 = self._dem.read(1)
                self._slope = rasterio.open(
                    f"/mnt/data/SIG/DEM/RGEALTI/RGEALTI_2-0_5M_ASC_LAMB93-IGN69_D0{id_dept}_SLOPE.tif"
                )
                self._slope_b1 = self._slope.read(1)
            obs["altitude_ign"] = obs.apply(
                lambda row: self.__alti(row.coord_x_local, row.coord_y_local),
                axis=1,
            )
            obs["pente_ign"] = obs.apply(
                lambda row: self.__slope(row.coord_x_local, row.coord_y_local),
                axis=1,
            )
            obs["ecart_vertical"] = obs.apply(
                lambda row: row.altitude - row.altitude_ign,
                axis=1,
            )
            obs["ecart_horizontal"] = obs.apply(
                lambda row: abs(row.ecart_vertical / max(row.pente_ign / 100, 0.1)),
                axis=1,
            )
            obs.to_csv(RESULTDIR / f"{id_dept}_{commune}_ecart.csv")
        return None